﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
	public int Money;
	public int startMoney = 400; 

	public int Lives;
	public int startLives = 20;

	void Start()
	{
		Money = startMoney;
		Lives = startLives;
	}

	void Update()
	{
		if(Lives <= 0)
		{
			Lives = 0;
			Time.timeScale = 0;

			if(Input.GetButtonDown("Fire1"))
			{
				Time.timeScale = 1;
				SceneManager.LoadScene("GameScene");
			}

		}
	}
}
