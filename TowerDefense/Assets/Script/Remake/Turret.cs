﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour 
{
	public Transform target;

	[Header("Attributes")]

	public int Damage = 10;

	public float
		chillDamage = 10, 
		chillTime = 5, 
		burnDamage = 30, 
		burnTime = 3;

	public float range = 15f;
	public float fireRate = 1f;
	private float fireCountdown = 0f;

	[Header("Unity Setup Fields")]

	public string enemyTag = "Enemy";

	public Transform partToRotate;
	public float turnSpeed = 10f;

	public GameObject bulletPrefab;
	public Transform firePoint;

	// Use this for initialization
	void Start () 
	{
		InvokeRepeating ("UpdateTarget", 0f, 0.5f);
	}

	void UpdateTarget()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag (enemyTag);
		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;
		foreach(GameObject enemy in enemies)
		{
			float distanceToEnemy = Vector3.Distance (transform.position, enemy.transform.position);
			if(distanceToEnemy < shortestDistance)
			{
				shortestDistance = distanceToEnemy;
				nearestEnemy = enemy;
			}
		}

		if (nearestEnemy != null && shortestDistance <= range) {
			target = nearestEnemy.transform;
		} else
			target = null;
	}
		
	void Update () 
	{
		if (target == null)
			return;

		LockOnTarget ();

		if(fireCountdown <= 0f)
		{
			Shoot ();
			fireCountdown = 1f / fireRate;
		}

		fireCountdown -= Time.deltaTime;

	}

	void LockOnTarget()
	{
		Vector3 dir = target.position - transform.position;
		Quaternion lookRotation = Quaternion.LookRotation (dir);
		Vector3 rotation = Quaternion.Lerp (partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
		partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
	}

	void Shoot()
	{
		GameObject bulletGO = (GameObject)Instantiate (bulletPrefab, firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGO.GetComponent<Bullet> ();

		bullet.damage = Damage;

		if(bulletGO.GetComponent<ChillBullet> () )
		{
			bulletGO.GetComponent<ChillBullet> ().slowOT= chillDamage; 
			bulletGO.GetComponent<ChillBullet> ().Duration = chillTime;
		}

		if(bulletGO.GetComponent<BurnBullet> () )
		{
			bulletGO.GetComponent<BurnBullet> ().burnPS= burnDamage; 
			bulletGO.GetComponent<BurnBullet> ().Duration = burnTime;
		}

		if (bullet != null)
			bullet.Seek (target);
		
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, range);
	}
}
