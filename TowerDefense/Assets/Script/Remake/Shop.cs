﻿using UnityEngine;

public class Shop : MonoBehaviour 
{
	public TurretBlueprint Cannon;
	public TurretBlueprint Crossbow;
	public TurretBlueprint Slow;
	public TurretBlueprint Burn;

	BuildManager buildManager;

	void Start()
	{
		buildManager = BuildManager.instance;
	}

	public void SelectCannonTurret()
	{
		Debug.Log ("Purchased Standard Turret");
		buildManager.SelectTurretToBuild (Cannon);
	}

	public void SelectCrossbowTurret()
	{
		Debug.Log ("Purchased Another Turret");
		buildManager.SelectTurretToBuild (Crossbow);
	}

	public void SelectSlowTurret()
	{
		Debug.Log ("Purchased Standard Turret");
		buildManager.SelectTurretToBuild (Slow);
	}

	public void SelectBurnTurret()
	{
		Debug.Log ("Purchased Another Turret");
		buildManager.SelectTurretToBuild (Burn);
	}

}
