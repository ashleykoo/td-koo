﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour 
{
	public Color hoverColor;
	public Color notEnoughMoneyColor;
	public Vector3 positionOffset;

	[HideInInspector]
	public GameObject turret;
	[HideInInspector]
	public TurretBlueprint turretBlueprint;
	[HideInInspector]
	public bool isUpgraded = false;

	private Renderer rend;
	private Color startColor;

	BuildManager buildManager;

	public PlayerStats playerStats;

	void Start()
	{
		rend = GetComponent<Renderer> ();
		startColor = rend.material.color;

		buildManager = BuildManager.instance;

		if (playerStats == null)
			playerStats = GameObject.Find("GameMaster").GetComponent<PlayerStats>();
	}

	public Vector3 GetBuildPosition()
	{
		return transform.position + positionOffset;
	}

	void OnMouseDown()
	{
		if (EventSystem.current.IsPointerOverGameObject ())
			return;

		if(turret != null)
		{
			buildManager.SelectNode (this);
			return;
		}

		if (!buildManager.CanBuild)
			return;
		
		BuildTurret (buildManager.GetTurretToBuild ()); 
	}

	void BuildTurret(TurretBlueprint blueprint)
	{
		if(playerStats.Money < blueprint.cost)
		{
			Debug.Log ("Not enough money to build!");
			return;
		}

		playerStats.Money -= blueprint.cost;

		GameObject _turret = (GameObject) Instantiate (blueprint.prefab, GetBuildPosition (), Quaternion.identity);
		turret = _turret;

		turretBlueprint = blueprint;

		Debug.Log("Turret build!");
	}

	public void UpgradeTurret()
	{
		if(playerStats.Money < turretBlueprint.upgradeCost)
		{
			Debug.Log ("Not enough money to upgrade!");
			return;
		}

		playerStats.Money -= turretBlueprint.upgradeCost;

		//Get rid of the old turret
		Destroy (turret);

		//Build a new one
		GameObject _turret = (GameObject) Instantiate (turretBlueprint.upgradedPrefab, GetBuildPosition (), Quaternion.identity);
		turret = _turret;

		isUpgraded = true;

		Debug.Log("Turret upgraded!");
	}

	void OnMouseEnter()
	{
		if (EventSystem.current.IsPointerOverGameObject ())
			return;

		if (!buildManager.CanBuild)
			return;

		if(buildManager.HasMoney)
		{
			rend.material.color = hoverColor;
		} else
		{
			rend.material.color = notEnoughMoneyColor;
		}
		

	}

	void OnMouseExit()
	{
		rend.material.color = startColor;
	}

}
