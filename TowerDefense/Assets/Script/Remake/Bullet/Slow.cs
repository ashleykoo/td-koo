﻿using UnityEngine;
using System.Collections;

public class Slow : Debuff 
{
	public float startSpeed;
	public float slowMovement;

	public override void Activate ()
	{
		GetComponent<NavMeshAgent> ().speed *= slowMovement;
	}
}
