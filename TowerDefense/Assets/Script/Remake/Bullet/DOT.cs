﻿using UnityEngine;
using System.Collections;

public class DOT : Debuff 
{
	public float Burn;

	public override void Activate ()
	{
		base.Activate ();
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Enemy> ().TakeDamage (Burn * Time.deltaTime);
	}
}
