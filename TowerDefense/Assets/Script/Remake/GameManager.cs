﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	public PlayerStats playerStats;

	private bool gameEnded = false;

	void Start()
	{
		if (playerStats == null)
			playerStats = GameObject.Find("GameMaster").GetComponent<PlayerStats>();
	}
	void Update () 
	{
		if(playerStats.Lives <= 0)
		{
			EndGame ();
		}

		if (gameEnded)
			return;
	}

	void EndGame()
	{
		gameEnded = true;
		print ("Game Over");
	}
}
