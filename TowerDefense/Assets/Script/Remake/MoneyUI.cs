﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyUI : MonoBehaviour 
{
	public Text moneyText;
	public PlayerStats playerStats;

	void Start()
	{
		if(playerStats == null)
		{
			playerStats = GameObject.Find("GameMaster").GetComponent<PlayerStats>();
		}
	}

	// Update is called once per frame
	void Update () 
	{
		moneyText.text = "$" + playerStats.Money.ToString();
	}
}