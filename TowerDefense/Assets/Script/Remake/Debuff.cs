﻿using UnityEngine;
using System.Collections;

public class Debuff : MonoBehaviour 
{
	public float
		Duration,
		timeElapsed;

	public virtual void Activate()
	{

	}
		
	// Update is called once per frame
	void Update () 
	{
		timeElapsed += Time.deltaTime;
		Activate ();

		if(timeElapsed >= Duration)
		{
			Destroy (this);
		}
	}


}
