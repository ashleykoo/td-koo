﻿using UnityEngine;
using System.Collections;

public class BurnBullet : Bullet 
{
	public float burnPS;
	public float Duration;

	public override void OnHit (Transform enemy)
	{
		Enemy e = enemy.GetComponent<Enemy> ();

		if(e != null)
		{
			e.TakeDamage (damage);
			if (enemy.GetComponent <DOT> ())
			{
				DOT dot = enemy.GetComponent<DOT> ();
				dot.Duration = Duration;
				dot.Burn = burnPS;


			}
			else 
			{
				e.gameObject.AddComponent <DOT>();
				DOT dot = enemy.GetComponent<DOT> ();
				dot.Duration = Duration;
				dot.Burn = burnPS;
			}

		}
		Destroy (gameObject);
	}
}
