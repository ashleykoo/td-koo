﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LivesUi : MonoBehaviour 
{
	public Text livesText;
	public PlayerStats playerStats;

	void Start () 
	{
		if(playerStats == null)
		{
			playerStats = GameObject.Find("GameMaster").GetComponent<PlayerStats>();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		livesText.text = "Health: " + playerStats.Lives.ToString();
	}
}
