﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{
	private bool doMovement = true;

	public float scrollSpeed = 5f;
	public float minY = 10f;
	public float maxY = 40f;
	
	public float Speed;
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
			doMovement = !doMovement;
		if (!doMovement)
			return;

		if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
		{
			transform.position -= Vector3.right * Speed * Time.deltaTime;
		}

		if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
		{
			transform.position += Vector3.right * Speed * Time.deltaTime;
		}

		if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
		{
			transform.position += Vector3.forward * Speed * Time.deltaTime;
		}

		if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
		{
			transform.position -= Vector3.forward * Speed * Time.deltaTime;
		}


		float scroll = Input.GetAxis ("Mouse ScrollWheel"); 
		Vector3 pos = transform.position;

		pos.y -= scroll * 1000 * scrollSpeed * Time.deltaTime;
		pos.y = Mathf.Clamp (pos.y, minY, maxY); 

		transform.position = pos;
	}
} 

