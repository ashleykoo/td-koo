﻿using UnityEngine;
using System.Collections;

public class ChillBullet : Bullet 
{
	public float slowOT;
	public float Duration;

	public override void OnHit (Transform enemy)
	{
		Enemy e = enemy.GetComponent<Enemy> ();

		if(e != null)
		{
			e.TakeDamage (damage);
			if (enemy.GetComponent <Slow> ())
			{
				Slow slow = enemy.GetComponent<Slow> ();
				slow.Duration = Duration;
				slow.slowMovement = slowOT;


			}
			else 
			{
				e.gameObject.AddComponent <Slow>();
				Slow slow = enemy.GetComponent<Slow> ();
				slow.Duration = Duration;
				slow.slowMovement = slowOT;
			}
				
		}
		Destroy (gameObject);
	}
}
