﻿using UnityEngine;
using System.Collections;

public class BuildManager : MonoBehaviour 
{
	public static BuildManager instance;
	public PlayerStats playerStats;

	void Awake()
	{
		if(instance != null)
		{
			Debug.Log ("More than one BuildManager in scene!");
		}
		instance = this;

		if (playerStats == null)
			playerStats = GameObject.Find("GameMaster").GetComponent<PlayerStats>();

	}

//	public GameObject CannonPrefab;
//	public GameObject CrossbowPrefab;

	private TurretBlueprint turretToBuild;
	private Node selectedNode;

	public NodeUI nodeUI;

	public bool CanBuild {get { return turretToBuild != null;} }
	public bool HasMoney {get { return playerStats.Money >= turretToBuild.cost;} }

	public void SelectNode(Node node)
	{
		if(selectedNode == node)
		{
			DeselectNode ();
			return;
		}

		selectedNode = node;
		turretToBuild = null;

		nodeUI.SetTarget (node);
	}

	public void DeselectNode()
	{
		selectedNode = null;
		nodeUI.Hide ();
	}

	public void SelectTurretToBuild(TurretBlueprint turret)
	{
		turretToBuild = turret;
		DeselectNode ();
	}

	public TurretBlueprint GetTurretToBuild()
	{
		return turretToBuild;
	}
}
