﻿using UnityEngine;

public class Bullet : MonoBehaviour 
{
	private Transform target;

	public float speed = 70f;
	public float explosionRadius = 0f;

	public float damage = 50;

//	public int reward = 10;

	public void Seek(Transform _target)
	{
		target = _target;
	}
		
	void Update () 
	{
		if(target == null)
		{
			Destroy (gameObject);
			return;
		}

		Vector3 dir = target.position - transform.position;
		float distanceThisFrame = speed * Time.deltaTime;

		if(dir.magnitude <= distanceThisFrame)
		{
				OnHit (target);
				return;
		}

		transform.Translate (dir.normalized * distanceThisFrame, Space.World);
		transform.LookAt (target);
	}

	public virtual void OnHit(Transform enemy)
	{
		Enemy e = enemy.GetComponent<Enemy> ();

		if(e != null)
		{
			e.TakeDamage (damage);
		}

		Destroy (gameObject);
	}

	void HitTarget()
	{

		if(explosionRadius > 0f)
		{
			Explode ();
		} else
		{
			Damage (target);
		}

		Destroy (gameObject);
	}

	void Explode()
	{
		Collider[] colliders = Physics.OverlapSphere (transform.position, explosionRadius);
		foreach(Collider collider in colliders)
		{
			if(collider.tag == "Enemy")
			{
				Damage (collider.transform);
			}
		}
	}

	void Damage (Transform enemy)
	{
		Enemy e = enemy.GetComponent<Enemy> ();

		if(e != null)
		{
			e.TakeDamage (damage);
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, explosionRadius);
	}
}