﻿using UnityEngine;
using System.Collections;

public enum MonsterTypes{Ground, Flying}

public class Enemy : MonoBehaviour 
{
	public MonsterTypes Type;

	NavMeshAgent nav;
	public Transform Point;
	private GameObject objSpawn;
	private int SpawnerID;

	public float Health = 100;

	public int EnemyValue = 50;

	public int EnemyDamage = 2;


	public PlayerStats playerStats;


	void Start () 
	{
		nav = GetComponent<NavMeshAgent> ();
		nav.SetDestination (Point.position);

		if (playerStats == null)
			playerStats = GameObject.Find("GameMaster").GetComponent<PlayerStats>();

	}


	public void TakeDamage(float amount)
	{
		Health -= amount;
		if(Health <= 0f)
		{
			Die ();
		}
	}

	void Die()
	{
		playerStats.Money += EnemyValue;
		Destroy(gameObject);
	}

	void OnTriggerEnter(Collider other)
	{

		if (other.gameObject.tag == "Base") 
		{
			Endpath ();
		}
	}

	void Endpath()
	{
		playerStats.Lives -= EnemyDamage;
		Destroy (this.gameObject);
	}
}
