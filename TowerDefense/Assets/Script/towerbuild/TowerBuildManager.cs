﻿using UnityEngine;
using System.Collections;

public class TowerBuildManager : MonoBehaviour {

	public GameObject Tower;

	void Start()
	{
		Tower = null;
	}

	public void TowerType(GameObject selectedTower)
	{
		Tower = selectedTower;
	}
}
